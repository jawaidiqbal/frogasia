# FrogAsia Technical Test

**Thank you for giving me a chance to apply for a developer position at FrogAsia.** 

In this test, i have used Laravel 5.3 framework. You can check a working demo at: http://newproject-jawaid.codeanyapp.com/

## How to Use
This project contains a Frog Pond. You will find all the frogs, added by users. Currently this system doesn't need any User Authentication to add frog, so its a public frog pond. You can click on "Add Frog" button at the top to add a frog. You need to provide name and gender for frog and that frog will appear randomly on pond. You can also do "Mating" between male and female frogs. Click on the "Do Mating" button and select male and female frogs. After mating is done, a child will be added to pond. This child name and gender depends randomly. You can hover on the child frog to know, hows child is this. Lastly, you can "Kill" a frog. Click on a frog, then a delete icon will appear. You can click on icon to confirm and kill a frog. Also whenever you move a frog, frog coordinates will updated, so next time you will find your pond on the same place, you left.

Enjoy and create some funny frogs. :)

## Official Documentation
Following are the process, involved in developing this project.

1) I downloaded a free template from internet. (https://html5up.net/landed)
2) I created/download images and jquery resources neccessary for this project.
3) I setup the routes, views and controllers related to Frog management.
4) FrogController contains all the neccessary functions needed to perform actions like Saving, Listing, Position Updates and Status Update (when you kill a frog).

Please note that all this is just to give a demo of my capabilities and i am not limited to only these expertise. I have done everything above in 2 days. So many other features can be added like User Authenticating, Redis (to cache the listing etc), Environment management (user can change background to his own pond) etc.

Thanks  


## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

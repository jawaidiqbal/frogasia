<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Frog;
use App\FrogLog;
use App\FrogMating;
use Illuminate\Http\Request;

class FrogController extends Controller {
	
	protected $request;

    public function __construct(Request $request)
    {

        $this->request = $request;
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function lists()
	{
        $frog = Frog::where('is_dead','no')->get();
        if($frog->count()){
			$data = array();
			$i =0;
			$frogs = $frog->toArray();
			foreach ($frogs as $f)
			{
				$data[$i]['frog_id'] = $f['frog_id'];
				$data[$i]['name'] = $f['name'];
				$data[$i]['gender'] = $f['gender'];
				$data[$i]['is_child'] = $f['is_child'];
				if ($f['is_child']=='yes')
				{
					$frogMating = FrogMating::query();
					$frogMating->where("child_id",$f['frog_id']);
					$frogMatingRec = $frogMating->first();
					if ($frogMatingRec)
					{
						$frogMatingRec = $frogMatingRec->toArray();
						$male = Frog::find($frogMatingRec['first_frog_id']);
						$female = Frog::find($frogMatingRec['second_frog_id']);
						$data[$i]['father'] = $male->name;
						$data[$i]['mother'] = $female->name;
					}
				}
				$data[$i]['position_cordinates'] = json_decode($f['position_cordinates'],true);
				$i++;
			}
            $message = array('header'=>'200','status'=>'success','data'=>$data);
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}


	    /**
	     * Store a newly created resource in storage.
	     *
	     * @param Request $request
	     * @return Response
	     */
	public function store(Request $request)
	{
        $fields = $request->all();
        $v_rule = array('name' => 'required');
        $v = \Validator::make($fields, $v_rule);//validation
        if ($v->fails()) {
            $message = array('header'=>'400','status'=>'error','msg'=>'invalid parameters');
        }else{
			
			$position_cordinates = array();
			$position_cordinates['x'] = rand(10,100); // Left
			$position_cordinates['y'] = rand(10,100); // Top
			$fields['position_cordinates'] = json_encode($position_cordinates);
            //$frog = Frog::register($fields)->save();
			$frog = new Frog();
			$frogId = $frog->insertGetId(['name' => $fields['name'],'gender' => $fields['gender'],'position_cordinates' => $fields['position_cordinates']]);
            $message = array('header'=>'200','status'=>'success','position_cordinates'=>$position_cordinates,'frog_id'=>$frogId);
        }
        return json_encode($message);
	}
	
	/**
	     * Store a newly created resource in storage.
	     *
	     * @param Request $request
	     * @return Response
	     */
	public function mating(Request $request)
	{
        $fields = $request->all();
        $v_rule = array('frog_male' => 'required','frog_female' => 'required');
        $v = \Validator::make($fields, $v_rule);//validation
        if ($v->fails()) {
            $message = array('header'=>'400','status'=>'error','msg'=>'invalid parameters');
        }else{
			$male = Frog::find($fields['frog_male']);
			$female = Frog::find($fields['frog_female']);
			
			$genderId = rand(0,1);
			if ($genderId==0) $gender = 'male';
			else $gender = 'female';
			if ($gender=='male') $name = "Little ".$male->name;
			else if ($gender=='female') $name = "Little ".$female->name;
			
			// Lets do the mating process
			$position_cordinates = array();
			$position_cordinates['x'] = rand(10,100); // Left
			$position_cordinates['y'] = rand(10,100); // Top
			$fields['position_cordinates'] = json_encode($position_cordinates);			
			$childfrog = new Frog();
			$childId = $childfrog->insertGetId(['name' => $name,'gender' => $gender,'is_child'=>'yes','position_cordinates' => $fields['position_cordinates']]);

			$frogmating_fields = [
				'first_frog_id'=>$fields['frog_male'],
				'second_frog_id'=>$fields['frog_female'],
				'child_id'=>$childId
			];
			$frogMating = new FrogMating();
			$frogMating->register($frogmating_fields)->save();
		
            $message = array('header'=>'200','status'=>'success','position_cordinates'=>$position_cordinates,'frog_id'=>$childId);
        }
        return json_encode($message);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $frog = Frog::find($id);
        if($frog){
            $message = array('header'=>'200','status'=>'success','data'=>$frog->toArray());
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = \Request::all();
        $frog = Frog::find($id);
        if($frog){
            $frog->fill($data);
            $frog->save();
            $message = array('header'=>'200','status'=>'success');
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}
	
	/**
	 * Update the frog position
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function positionupdate(Request $request)
	{
		$fields = $request->all();
        $frog = Frog::find($fields['id']);
        if($frog){
            $position_cordinates = array();
			$position_cordinates['x'] = $fields['x'];
			$position_cordinates['y'] = $fields['y'];
			$frog->position_cordinates = json_encode($position_cordinates);
            $frog->save();
            $message = array('header'=>'200','status'=>'success');
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}
	
	/**
	 * Kill the frog
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function status(Request $request)
	{
		$fields = $request->all();
        $frog = Frog::find($fields['id']);
        if($frog){            
			$frog->is_dead = 'yes';
            $frog->save();
			
			$froglog_fields = [
				'frog_id'=>$fields['id'],
				'action'=>'kill'
			];
			$frogLog = new FrogLog();
			$frogLog->register($froglog_fields)->save();
			
            $message = array('header'=>'200','status'=>'success');
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $frog = Frog::find($id);
        if($frog){
            $frog->delete();
            $message = array('header'=>'200','status'=>'success');
        }else{
            $message = array('header'=>'400','status'=>'error','msg'=>'no record found');
        }
        return json_encode($message);
	}

}

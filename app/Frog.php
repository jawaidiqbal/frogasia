<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Frog extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'frog';

    protected $guarded = [''];

    public $timestamps = true;
	
	protected $primaryKey = 'frog_id';

    public static function register(array $attribute)
    {
        return new static($attribute);
    }
}

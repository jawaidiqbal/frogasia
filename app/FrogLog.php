<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class FrogLog extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'frog_log';

    protected $guarded = [''];

    public $timestamps = true;
	
	protected $primaryKey = 'log_id';

    public static function register(array $attribute)
    {
        return new static($attribute);
    }
}

<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class FrogMating extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'frog_mating';

    protected $guarded = [''];

    public $timestamps = true;
	
	protected $primaryKey = 'id';

    public static function register(array $attribute)
    {
        return new static($attribute);
    }
}

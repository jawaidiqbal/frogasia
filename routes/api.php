<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('frog/list', 'FrogController@lists');
Route::post('frog/status', 'FrogController@status');
Route::post('frog/mating', 'FrogController@mating');
Route::post('frog/position', 'FrogController@positionupdate');
Route::post('frog/save', 'FrogController@store');

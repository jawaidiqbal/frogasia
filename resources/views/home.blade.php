<!DOCTYPE HTML>
<html>
<head>
<title>FrogAsia :: Frog Pond</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/jquery.modal.css" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="landing">
<div id="ex2" style="display:none;" class="modal">
  <form action="" class="login_form ">
    <h3>Add New Frog</h3>
    <p>
      <label>Name:</label>
      <input type="text" id="frog_name" name="frog_name" placeholder="Enter your frog name" />
    </p>
    <p>
      <label>Gender:</label>
      <select name="frog_gender" id="frog_gender">
	  	<option value="male">Male</option>
		<option value="female">Female</option>
	  </select>
    </p>
    <p>		
      <input type="button" value="Submit" class="submit_frog" /> 
	  <span class="msg"></span>
    </p>
  </form>
</div>
<div id="ex3" style="display:none;" class="modal">
  <form action="" class="login_form ">
    <h3>Let's Do Mating</h3>
	<p>
      <label>Male:</label>
      <select name="frog_male" id="frog_male">
	  	<option value="0">Select</option>
		
	  </select>
    </p>
	<p>
      <label>Female:</label>
     <select name="frog_female" id="frog_female">
	  	<option value="0">Select</option>
		<option value="female">Female</option>
	  </select>
    </p>
	<p>		
      <input type="button" value="Do Mating" class="submit_mating" /> 
	  <span class="mating_msg"></span>
    </p>
  </form>
</div>
<div id="ex4" style="display:none;" class="modal">
  <form action="" class="login_form ">
    <h3>It's Death Time</h3>
	<p> Please confirm you want to kill this frog?</p>
	<p style="text-align:center;">		
      <input type="button" value="Kill Now" class="submit_delfrog" />
	  <input type="hidden" id="del_frog_id" name="del_frog_id" value="0" /> 
	  <span class="del_msg"></span>
    </p>
  </form>
</div>
<div id="page-wrapper">
  <!-- Header -->
  <header id="header">
    <h1 id="logo" ><a href=""><img src="images/logo.png" class="logo"></a></h1>
    <nav id="nav">
      <ul>
        <li><a class="button special btn_addfrog" href="#ex2">Add Frog</a></li>
		<li><a class="button special btn_mating" style="cursor:pointer; background-color:#000;">Do Mating</a></li>
      </ul>
    </nav>
  </header>
  <!-- Banner -->
  <section id="banner">
    <div class="content" style="width:100%; text-align:center;">
      <header>
        <h2 style="font-weight:600;">Hello! We are FrogAsia!</h2>
        <p style="font-weight:600;">This is my frog pond ! Add your frogs to my pond</p>
      </header>
      <br>
      <div id="myCanvas" style="height:400px; width:100%; position:relative;"></div>
    </div>
  </section>
</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>
<script src="assets/js/functions.js?timer=<?php echo date("ymdhis");?>"></script>
<script src="assets/js/jquery.modal.js"></script>
<script src="assets/js/jquery-ui.js"></script>
</body>
</html>

// This is main fuction class which contains all the applicant related functions for Frog Management - By Jawaid

var server          = window.location.hostname;
var canvas_url      = "";
if(server=='localhost')
    canvas_url          = location.protocol+"//localhost:8000/";
var frogsArray      = new Array();
var frogsArrayCounter = 0;
$(document).ready(function() {
	
	$('.btn_addfrog').click(function(event) {
	  event.preventDefault();
		$("#ex2").modal("open");
	});

	$(document).on("click", function(e) {
		var container = $(".item");

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$(".item").removeClass("itemselected");
			$(".item").removeClass("childselected");
		}
	
	  });
	
	$('.btn_mating').click(function(event) {
	  event.preventDefault();
		$("#ex3").modal("open");
		var males = '<option value="0">Select</option>';
		var females = '<option value="0">Select</option>';
		for (var i =0;i<frogsArray.length;i++)
		{
			if (frogsArray[i].is_child=='no')
			{
				if (frogsArray[i].gender=='male')
				males += '<option value="'+frogsArray[i].frog_id+'">'+frogsArray[i].name+'</option>';
				else if (frogsArray[i].gender=='female')
				females += '<option value="'+frogsArray[i].frog_id+'">'+frogsArray[i].name+'</option>';
			}
		}
		
		$("#frog_male").html(males);
		$("#frog_female").html(females);
	});
	
	$('.submit_mating').click(function() {
	  	if ($(".submit_mating").hasClass("disabled")) {
            return false;
		}
		else
		{
			var frog_male = $("#frog_male").val();
			var frog_female = $("#frog_female").val();
			if (frog_male=='0')
			{
				$(".mating_msg").html('Please select male frog ');
				setTimeout(function() {
				   $(".mating_msg").html('');
				}, 1000);
				$("#frog_male").focus();
				return false;
			}
			else if (frog_female=='0')
			{
				$(".mating_msg").html('Please select female frog ');
				setTimeout(function() {
				   $(".mating_msg").html('');
				}, 1000);
				$("#frog_female").focus();
				return false;
			}
			else
			{
				$(".mating_msg").html('Please wait...');
				$('.submit_mating').addClass("disabled");
				$.post(canvas_url+"api/frog/mating", {'frog_male':frog_male,'frog_female':frog_female,'_token':$('meta[name="csrf-token"]').attr('content'), '_method':"POST" }, function(data){
					if (data.status=='success')
					{
						$(".mating_msg").html('Congrats!! You got a baby.');
						$.getFrogs();
						setTimeout(function() {
							$("#frog_male").val('0');
							$("#frog_female").val('0');
							$('.submit_mating').removeClass("disabled");
							$(".mating_msg").html('');
						    $("#ex3").modal('close');
						    $(".close-modal").trigger("click");
						}, 1000);
					}
					else
					{
						$(".msg").html('Opps! There is some error');
					}
				},"json");
			}
		}		
	});
	
	$('.submit_frog').click(function() {
	  	if ($(".submit_frog").hasClass("disabled")) {
            return false;
		}
		else
		{
			var frog_name = $("#frog_name").val();
			var frog_gender = $("#frog_gender").val();
			if (frog_name=='')
			{
				$(".msg").html('Please enter frog name');
				setTimeout(function() {
				   $(".msg").html('');
				}, 1000);
				$("#frog_name").focus();
				return false;
			}
			else
			{
				$(".msg").html('Please wait...');
				$('.submit_frog').addClass("disabled");
				$.post(canvas_url+"api/frog/save", {'name':frog_name,'gender':frog_gender,'_token':$('meta[name="csrf-token"]').attr('content'), '_method':"POST" }, function(data){
					if (data.status=='success')
					{
						$(".msg").html('New Frog added to Pond!!');
						$.getFrogs();
						setTimeout(function() {
							$("#frog_name").val('');
							$('.submit_frog').removeClass("disabled");							
						   $(".msg").html('');
						   $("#ex2").modal('close');
						   $(".close-modal").trigger("click");
						}, 1000);
					}
					else
					{
						$(".msg").html('Opps! There is some error');
					}
				},"json");
			}
		}		
	});
	
	$('.submit_delfrog').click(function() {
	  	if ($(".submit_delfrog").hasClass("disabled")) {
            return false;
		}
		else
		{
			var del_frog_id = $("#del_frog_id").val();
			if (del_frog_id=='0')
			{
				$(".del_msg").html('Please select frog to kill ');
				setTimeout(function() {
				   $(".del_msg").html('');
				}, 1000);
				$("#del_frog_id").focus();
				return false;
			}
			else
			{
				$(".del_msg").html('Please wait...');
				$('.submit_delfrog').addClass("disabled");
				$.post(canvas_url+"api/frog/status", {'id':del_frog_id,'status':'delete','_token':$('meta[name="csrf-token"]').attr('content'), '_method':"POST" }, function(data){
					if (data.status=='success')
					{
						$(".del_msg").html('Ahhh!!!! You killed an innocent frog :(');
						$.getFrogs();
						setTimeout(function() {
							$("#del_frog_id").val('0');
							$('.submit_delfrog').removeClass("disabled");
							$(".del_msg").html('');
						    $("#ex4").modal('close');
						    $(".close-modal").trigger("click");
						}, 1000);
					}
					else
					{
						$(".msg").html('Opps! There is some error');
					}
				},"json");
			}
		}		
	});
	
	$.getFrogs = function ()
	{
		$.get(canvas_url+"api/frog/list", {'_token':$('meta[name="csrf-token"]').attr('content'), '_method':"GET" }, function(data){
			if (data.status=='success')
			{
				var frogs = data.data;
				var frogs_count = frogs.length;
				var resultDiv = '';
				var frog_name = '';
				var parents = '';
				for (var i =0;i<frogs_count;i++)
				{
					frogsArray[frogsArrayCounter] = frogs[i];
					frogsArrayCounter++;
					if (frogs[i].is_child=='yes')
					{
						parents = '';
						if (frogs[i].gender=='male') parents = 'Son of '+frogs[i].father+' and '+frogs[i].mother;
						else if (frogs[i].gender=='female') parents = 'Daughter of '+frogs[i].father+' and '+frogs[i].mother;
						resultDiv += '<div class="item" data-id="'+frogs[i].frog_id+'" data-type="child" title="'+parents+'" style="top:'+frogs[i].position_cordinates.y+'%; left:'+frogs[i].position_cordinates.x+'%;"><span class="childname">'+frogs[i].name+'<span class="delete_item" data-id="'+frogs[i].frog_id+'"></span></span><span class="child"></span></div>';
					}
					else
					{
						resultDiv += '<div class="item" data-id="'+frogs[i].frog_id+'" data-type="'+frogs[i].gender+'" style="top:'+frogs[i].position_cordinates.y+'%; left:'+frogs[i].position_cordinates.x+'%;"><span class="name">'+frogs[i].name+'<span class="delete_item" data-id="'+frogs[i].frog_id+'"></span></span><span class="'+frogs[i].gender+'"></span></div>';
					}
				}
				$("#myCanvas").html(resultDiv);
				$(".item").draggable({ containment: "#myCanvas", stop: function(d) {
					var id = $(this).attr("data-id");
					
					var x = parseInt($(this).css("left")) / ($("#myCanvas").width() / 100);
					var y = parseInt($(this).css("top")) / ($("#myCanvas").height() / 100);
					$(this).css("left",x+"%");
				    $(this).css("top",y+"%");		   
				  	
					$.post(canvas_url+"api/frog/position", {'id':id,'x':x,'y':y,'_token':$('meta[name="csrf-token"]').attr('content'), '_method':"POST" }, function(data){					
					},"json");
				} });
				
				$('.item').click(function(event) {
				  event.preventDefault();
				  	var gender = $(this).attr("data-type");
					var id = $(this).attr("data-id");
					$(".item").removeClass("itemselected");
					$(".item").removeClass("childselected");
					if (gender=='child')
					$(this).addClass("childselected");
					else
					$(this).addClass("itemselected");
					
				});
				
				$('.delete_item').click(function(event) {
				  event.preventDefault();				  	
					var id = $(this).attr("data-id");
					$("#del_frog_id").val(id);
					$("#ex4").modal("open");					
				});
			}
			else
			{
			
			}
		},"json");
	}
	
	$.getFrogs();	
});